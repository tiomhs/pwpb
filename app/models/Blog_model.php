<?php

 class Blog_model {
    private $table = 'blog';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllBlog(){
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }
    
    public function getBlogAndUserById($id)
    {
        $query = 'SELECT * FROM ' . $this->table . ' LEFT JOIN users on blog.id = users.idUser WHERE blog.id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function getAllBlogAndUser()
    {
        $query = 'SELECT * FROM ' . $this->table . ' LEFT JOIN users on blog.idUser = users.idUser';
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function delete($id)
    {
        $query = 'DELETE FROM ' . $this->table . ' WHERE id=:id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return 1;
    }

    public function tambahBlog($data){
        $this->db->query("INSERT INTO {$this->table} VALUES (null, :judul, :tulisan, :idUser)");
        $this->db->bind('idUser',$data['idUser']);
        $this->db->bind('judul',$data['judul']);
        $this->db->bind('tulisan',$data['tulisan']);
        $this->db->execute();
        return 1;
    }

    public function getBlogById($id)
    {
        $query = 'SELECT * FROM {$this->table} WHERE id=:id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function updateBlog($blogData, $id)
    {
        // echo "<pre>";
        // var_dump($blogData);die;
        // echo "</pre>";
        $query = 'UPDATE blog SET judul=:judul, tulisan=:tulisan, idUser=:idUser WHERE id=:id';
        $this->db->query($query);
        $this->db->bind('judul', $blogData['judul']);
        $this->db->bind('tulisan', $blogData['tulisan']);
        $this->db->bind('idUser', $blogData['idUser']);
        $this->db->bind('id', $id);
        $this->db->execute();
        return 1;
    }
 }