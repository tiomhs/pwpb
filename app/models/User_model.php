<?php 

class User_model{
    private $table = 'users';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function tampilUser($idUser){
        $this->db->query('SELECT * FROM $this->table WHERE idUser=:idUser');
        $this->db->bind('idUser', $idUser);
        return $this->db->resultSingle();
    }

    public function tampilSemua(){
        $query = "SELECT * FROM $this->table ";
        $this->db->query($query);
        return $this->db->resultAll();
    }
}