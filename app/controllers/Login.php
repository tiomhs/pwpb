<?php 

class Login extends Controller {
    public function index() {
        $this->view("login/index");
      }

    public function loginUser(){
      if($this->model('Auth_model')->login($_POST) > 0){
        $_SESSION["login"] = true;
        header("Location:".BASEURL."/home");
      }else {
        header("location:". BASEURL ."/login");
      }
    }

    public function logOut(){
        session_destroy();
        header('Location:' . BASEURL . '/login');
    }
}