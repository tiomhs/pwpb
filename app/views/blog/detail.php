<div class="container mt-3">
    <div class="card mb-3">
  <img src="<?= BASEURL; ?>/img/<?= $data['blog']['img']; ?>" class="card-img-top rounded" width="100">
  <div class="card-body">
    <h5 class="card-title"><?=$data['blog']['judul']?></h5>
    <p class="card-text"><small class="text-muted"><?=$data['blog']['Nama']?></small></p>
    <p class="card-text"><?=$data['blog']['tulisan']?></p>
  </div>
</div>
</div>