<?php 
  if(!$_SESSION['login']){
    header('location:'. BASEURL .'/login');
  }

  // echo "<pre>";
  // var_dump($_SESSION['login']);die;
  // echo "</pre>";
?>

<!DOCTYPE html>
<html lang="en">

 <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Halaman <?= $data["judul"]; ?></title>

   <link href="<?= BASEURL ?>/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?= BASEURL ?>/css/bootstrap.css" rel="stylesheet" >
 </head>

 <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       <div class="container"><a class="navbar-brand" href="<?= BASEURL ?>">MVC</a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
           aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
         <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
           <div class="navbar-nav">
            <a class="nav-item nav-link active" href="<?= BASEURL ?>">Home</a>
            <a class="nav-item nav-link" href="<?= BASEURL ?>/blog">Blog</a>
            <a class="nav-item nav-link" href="<?= BASEURL ?>/user">User</a>
            <a class="nav-item nav-link" href="<?= BASEURL ?>/login/logOut">LogOut</a></div>
         </div>
       </div>
     </nav>

 <body>
 